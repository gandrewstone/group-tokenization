# CHIP-2017-11-Group Tokenization for Bitcoin Cash

```
Title: Group Tokenization for Bitcoin Cash
Author: Anonymous Contributor A60AB5450353F40E
Date: 2021-02-21
Status: DRAFT
```

## Summary

The proposal updates the Bitcoin Cash consensus to natively support alternative
currencies without impeding on the utility of native currency (BCH) by upgrading
the peer-to-peer cash system of Bitcoin in such way that every other currency
transaction output created marginally increases demand for the native curreny
(BCH) thus remaining faithful to design precepepts from the Bitcoin whitepaper.

## Motivation

The first real use of Ethereum smart contracts was to implement tokens and from
there the market has seen a token cambrian explosion. Most tokens are used to
represent some form of cash. The market demands smart cash and the market got
smart contracts disguised as cash. We see this as an opportunity where Bitcoin
Cash could gain competitive advantage while staying true to the vision of the
best digital cash, thus improving utility and increasing the value of Bitcoin
Cash: The Peer-to-Peer Electronic Cash System for Planet Earth.

Smart contracts provide the verbs of a language, and tokens are the nouns. A
language cannot express much with just one noun but the reverse isn't true.
Tokens with only 4 verbs (create, transfer, mint, burn) are useful alone,
because they can express cash. Popularity of stablecoins has shown the demand
for that and those 4 verbs alone enabled blockchains to become more tightly
integrated with other financial systems. Smart contracts on top are mostly used
only to add security in interaction between currencies when interaction is to be
done in a trustless way.

Bitcoin Cash already has some verbs implemented through Script, but it has just
one noun. We believe that it needs more nouns in order to expand utility and
grow in value, that it needs smart cash with select specialized verbs before it
will need more general verbs. We believe those other nouns need to have the same
simplicity and ease of use as the first noun, and that the fundamental verbs
must be provided by the blockchain itself. After all, it's hard to make a
sentence if you're forced to use a combination of verbs as a substitute for
nouns.

## Implementation Costs and Risks

The solution is conceptually simple. The entire implementation showcasing it can
fit into a single `.cpp` file consisting of a single function using simple
language structures and which can fit well under ~400 lines of C++. By reusing
existing loops it can be even fewer lines affected. This kind of solution is
easy to implement and reason about.

Downstream, the users can rely on the same design patterns as Bitcoin uses which
have been successfully tested since 2009, and that will be demonstrated in the
**[Technical Description](#technical-description)** section of this document.

We cannot invent problems with this approach just to satisfy the reader looking
for problems. If we have missed something in our reasoning, we will be happy to
hear from you.

## Ongoing Costs and Risks

As above.

## Technical Description

An inescapeable fact is that currencies and balances have to be recorded. Any
design can only affect where they will be recorded. It is our view that the same
design precepts for storing the current, one, currency should apply to the many.

When thinking about multiple currencies what naturally comes to mind is to
generalize the protocol to support a `currency` field in the transaction format,
and treat all currencies the same. There are at least three problems with this
approach:

1. If a currency loses value, incentive to consolidate unspent outputs is also
lost.
2. Fee payment and block reward(s) become a more complex problem.
3. Economic incentives which drive security through an increase in hash-power
become a more complex problem.

Attemting to mitigate the above problems would only add to the complexity of
a such approach. For those reasons, we will not entertain that solution.

Instead, we propose an optional dual-currency output system, where the
balancing equation for the native currency is unchanged and the sums grouped by
other currencies must balance. In effect, the other currency is locked in such
dual-currency outputs.

![fig1](./CHIP-2017-11-fig1.png)

With this system the problems highlighted above cease to exist because:

1. There is incentive to merge even worthless other currency outputs to extract
the native currency from a group.
2. Fee mechanism is unaffected. Fees continue to be taken from the excess native
currency in a transaction.
3. The security model is unchanged. Every other currency creates marginal demand
for the native currency with each additional grouped output created and with
each transaction.

There is still the problem of managing the other currency total supply. Basic
management operations that any such dual-currency system requires are genesis,
mint, melt, and transfer of management authority. Burning can be done the same
way as with native currency: by consuming inputs in a provably unspendable way.
Melting is a new concept as it burns only the other currency and reclaims the
native currency, enabling it to fully escape the group.

For other currency management we introduce the concept of authority inputs and
outputs. Authority can be expressed by a bit flags field attached to the grouped
output, and then it can be split and merged just like any other quantity,
similarly to the figure above. The balancing is done by a logical sum which must
balance by group i.e. an authority not present in the grouped inputs cannot be
present in the grouped outputs. Presence of a mint or melt flag in a transaction
will allow relaxing the balancing constraint of the other currency, giving us a
way to create or destroy it.

With this we can easily manage the other currencies balance and the transfer
management authority. There is still the issue of genesis, how is the first
authority created?

Any amount of other currency must also carry a zero-knowledge proof of its
legitimacy, otherwhise the recipient would be burdened with verifying its
validity. The proof is achieved through a cryptographic witness of the genesis
transaction. Since we have to carry the witness with every dual-currency output,
and it will always be unique, the group id field can serve dual use, as a
witness and as the group identifier. In the genesis we reuse the last byte to
carry global group settings which could be used to place further restraints on
the group or to relax existing ones. An exotic use would be to allow anyone to
create both mint or melt outputs but such a group would not be a dual currency,
it would become a tagged native currency output.

Since the other currency functions the same as native currency, the above design
allows any Script to determine locking and unlocking of inputs and outputs. With
these nouns and verbs, we can achieve many things, such as scheduled issuance
and atomic swaps straight out of the box. The detailed description of how to
achieve each is beyond the scope of this document but the astute reader could
already be getting some ideas.

The above design is future-proof as it starts with a minimum set of constraints
to make another currency viable. Any future change will only enable further
constraints to be toggled through the group setting and new authority flags.
Further utility can be achieved by extending the Script functionality as well.

### Design Process Timeline

2017-10-16 Introduction

2017-11-19 BUIP

...

We could add snapshots of the past ideas and the path it took to get us here.
After all, the idea converged towards this design through multiple cycles.
Would be good to have it all referenced somewhere.

## Implementations

https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/NextChain/src/consensus/grouptokens.cpp

### Specification

[Simplified technical requirements](https://bitcoincashresearch.org/t/native-group-tokenization/278/28)

(Reference a current version controlled [specification](https://www.bitcoinunlimited.net/grouptokenization/groupbchspec) snapshot.)

### Test Cases

https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/NextChain/src/test/grouptoken_tests.cpp

https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/NextChain/qa/rpc-tests/grouptokens.py

## Evaluation of Alternatives

We see not having the feature as the only alternative. Other token proposals are
implemented with smart contracts therefore not on the same utility level as
native currency and are conceptually different from how the native currency
operates. Our proposal conceptually operates the same way as native currency so
downstream developers should not have too much trouble implementing it if
they're already familiar with how Bitcoin works.

Other smart contract proposals could operate on grouped tokens, and therefore
benefit from grouped tokens, because group tokens provide built-in nouns and
a select set of low level verbs.

More generally, we see alternatives in only the rollout approach:

1. We enable native tokens through this proposal,
2. Smart contract solutions either roll their own tokens or use native tokens;

as opposed to

1. Roll out a smart contract platform that can roll their own tokens,
2. Wait for the market to demand something more efficient, and then enable
native tokens.

The latter approach has a significantly bigger cost in:

- Missed opportunity in the time it takes to build smart contract token systems
and evaluate the market response.
- Technical debt of downstream users having to adapt to new design patterns
introduced by the smart contract platform.

We can wait for the market, but the market won't wait for us, and we have to
anticipate that demand if we're to get ahead.

We could also roll it out simultaniously, and synergetic benefit could be
achieved right from the start if the smart contracts would be designed with
features that can fully use group tokens.

## Security Considerations

## Stakeholder Cost and Benefits Analysis

We present a stakeholder taxonomy. Everyone should be able to find themselves
within at least one group. We attempt to evaluate the impact of this development
on all stakeholders and we invite you to join us, add your names to this list,
and comment whether you agree with our assesment of the impact. We're happy to
address any concerns or questions you may have.

General costs and benefits:

- (+) Competitive advantage in comparison to other blockchains/ecosystems.
- (+) This functionality will enable competition from within our ecosystem.
Some may feel threatened by this, but what is the alternative? To have the
competition build outside of our ecosystem and let them create value elsewhere?
- (+) Attracting new users, talent, and capital from outside thus helping grow
the entire ecosystem centered around Bitcoin Cash. Conceptually simple tools
mean more potential builders.
- (+) Opportunity cost. Success of tokens is a good measure of that. Missed
"revenue" in terms of user, talent, and capital inflows. Missed ecosystem growth
opportunity.
- (+) Future proof. Since every group token is also a BCH output, it should
seamlessly work with everything that can work with BCH, including Script and any
future smart contract platform.
- (+) Every grouped output created also creates marginal demand for BCH.
- (+) Increased Bitcoin Cash adoption and utility.
- (-) Potential to speed up adoption. While this benefits BCH, it could create
problems of its own. We don't see this as a realistic outcome, but this
perspectiveit is worth consideration, and it would be a good problem to have.

### 1 Node Stakeholders

- (-) Marginally more resource usage when compared to the simplest BCH UTXO.

#### 1.1 Miners

- (+) More fee revenue through increased demand for transactions.

#### 1.2 Big nodes

Exchanges, block explorers, backbone nodes, etc.

- (+) Easy to implement other currencies through same familiar design patterns
that are now used for Bitcoin Cash alone.

#### 1.3 User nodes

Hobbyst full nodes.

#### 1.4 Node developers

- (-) One-off workload increase
- (+) Later it just works out of the box, downstream developers create utility.

### 2 Userspace stakeholders

- (+) Barrier to entry to token usage lowered.

#### 2.1 Business users

- (+) Access to financial instruments previously not available, or access them
at a better price and quality than competition can provide.
- (+) Enables potential new business models.

#### 2.2 Userspace developers

- (+) Easy to implement other currencies through same familiar design patterns
that are now used for Bitcoin Cash alone.
- (+) With more growth there will be more opportunity for professional
advancement within the ecosystem.

#### 2.3 Individual users

- (+) Get to enjoy the best peer-to-peer electronic cash system!

### 3 Holders and speculators

#### 3.1 Long

- (+) More adoption correlates with increase in price.
- (+) Even other tokens marginally increase demand for BCH, so the more
transactions of any kind, the better.

#### 3.2 Short

- (+) More liquidity in the market. Following Bitcoin Cash may help you decide
to switch side and benefit from it!

### 4 Opinion holders who are not stakeholders

We are hoping developments like this will move you towards becoming
stakeholders!

## Statements

## Future Work

[Full future functional specification and upgrade path.](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs/edit)

## Copyright

MIT?
